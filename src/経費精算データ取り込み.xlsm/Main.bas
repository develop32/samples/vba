Attribute VB_Name = "Main"
Option Explicit

Public Sub ImportExpenses()

    Call ExpensesSheet.ClearTable
    
    Dim targetFiles As files
    Set targetFiles = GetFiles(ThisWorkbook.Path & "\data\")
    Dim file As file
    For Each file In targetFiles
        Dim wb As Workbook
        Set wb = Workbooks.Open(file.Path)
        Dim ws As Worksheet
        Set ws = wb.Worksheets(1)
        
        Call SetExpenses(ws)
        
        wb.Close
    Next

End Sub

Public Function GetFiles(ByVal directory As String) As files
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
    Set GetFiles = fso.GetFolder(ThisWorkbook.Path & "\data\").files
End Function

Public Sub SetExpenses(ByVal ws As Worksheet)
        Dim targetMonth As String: targetMonth = Format(ws.Range("ÎÛ").Value, "yyyyNmm")
        Dim departmentNo As String: departmentNo = "todo"
        Dim departmentName As String: departmentName = ws.Range("®").Value
        Dim employeeNo As String: employeeNo = ws.Range("ÐõNo").Value
        Dim employeeName As String: employeeName = ws.Range("¼").Value
        
        Dim table As ListObject: Set table = ws.ListObjects("oïe[u")
        Dim row As Excel.ListRow
        For Each row In table.ListRows
            Dim expense As ExpenseEntity
            Set expense = New ExpenseEntity: Set expense = expense.Init(targetMonth, _
                                                                        departmentNo, _
                                                                        departmentName, _
                                                                        employeeNo, _
                                                                        employeeName, _
                                                                        row.Range)
            
            ExpensesSheet.AddRowToTable expense
        Next
End Sub
