VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ExpenseEntity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public targetMonth As String
Public departmentNo As String
Public departmentName As String
Public employeeNo As String
Public employeeName As String
Public TargetDate As Date
Public Subject As String
Public Summary As String
Public Amount As Currency
Public Remarks As String

Public Function Init(ByVal pTargetMonth As String, _
                     ByVal pDepartmentNo As String, _
                     ByVal pDepartmentName As String, _
                     ByVal pEmployeeNo As String, _
                     ByVal pEmployeeName As String, _
                     ByVal pExpenseRow As Range) As ExpenseEntity
    Set Init = Me
    
    targetMonth = pTargetMonth
    departmentNo = pDepartmentNo
    departmentName = pDepartmentName
    employeeNo = pEmployeeNo
    employeeName = pEmployeeName
    TargetDate = pExpenseRow(1)
    Subject = pExpenseRow(2)
    Summary = pExpenseRow(5)
    Amount = pExpenseRow(6)
    Remarks = pExpenseRow(7)
End Function
