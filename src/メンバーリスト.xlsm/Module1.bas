Attribute VB_Name = "Module1"
Public Sub MySub()
    Dim people As Persons: Set people = New Persons
    
    With people
        .Add (Array("a04", "Jay", "male", #7/7/1995#))
        .Remove 2
        With .Items("a01")
            .FirstName = "Bomb"
            .Birthday = #11/11/1993#
        End With
        
        .ToSheet
    End With
    
    Stop
End Sub
