VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Persons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public items_ As Collection

Public Property Get Items(ByVal key As Variant) As person
    Set Items = items_.Item(key)
End Property

Private Sub Class_Initialize()
    Set items_ = New Collection
    
    With Sheet1
        Dim rowIndex As Long: rowIndex = 2
        Do While .Cells(rowIndex, 1).Value <> ""
            Me.Add .Range(.Cells(rowIndex, 1), .Cells(rowIndex, 4))
            rowIndex = rowIndex + 1
        Loop
    End With
End Sub

Public Function Add(ByVal values As Variant) As person
    Dim person As person: Set person = New person
    person.Initialize values
    items_.Add person, person.Id
    Set Add = person
End Function

Public Sub Remove(ByVal key As Variant)
    items_.Remove key
End Sub

Public Sub ToSheet()
    With Sheet1
        .Cells.Clear
        .Range(.Cells(1, 1), .Cells(1, 4)) = Array("Id", "FirstName", "Gender", "Birthday")
        
        Dim i As Long: i = 2
        Dim p As person
        For Each p In items_
            .Range(.Cells(i, 1), .Cells(i, 4)) = Array(p.Id, p.FirstName, p.Gender, p.Birthday)
            i = i + 1
        Next p
    End With
End Sub
