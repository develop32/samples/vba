VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Person"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private id_ As String
Public FirstName As String
Public Gender As String
Public Birthday As Date

Public Property Get Id() As String
    Id = id_
End Property

Public Property Let Id(ByVal newId As String)
    If id_ <> "" Then
        Debug.Print "Idは上書きすることはできません。"
    Else
        id_ = newId
    End If
End Property


Public Property Get IsMale() As Boolean
    IsMale = (Me.Gender = "male")
End Property


Public Sub Initialize(ByVal values As Variant)
    Select Case TypeName(values)
        Case "Range"
            id_ = values(1).Value
            FirstName = values(2).Value
            Gender = values(3).Value
            Birthday = values(4).Value
        Case "Variant()"
            id_ = values(0)
            FirstName = values(1)
            Gender = values(2)
            Birthday = values(3)
    End Select
End Sub

Public Sub Greet()
    MsgBox Me.FirstName & "です。こんにちは！"
End Sub
