Attribute VB_Name = "Main"
Option Explicit

''' 請求書を作成する。
Public Sub CreateInvoce()
    TemplateSheet.DayCutoff = Application.InputBox("年月を入力してください", "対象年月を入力", Format(Date, "yyyy/mm"))
    Debug.Print TemplateSheet.DayCutoff
    
    ClientsSheet.Store
    Dim myClients As Collection: Set myClients = ClientsSheet.Clients
    
    InvoicesSheet.Store
    Dim myInvoices As Collection: Set myInvoices = InvoicesSheet.Invoices
    
    Dim c As Client, d As Invoice
    For Each c In ClientsSheet.Clients
    
        Dim targetData As Collection: Set targetData = New Collection 'ひな形に貼り付けるデータのコレクション
        For Each d In InvoicesSheet.Invoices
            If EqualsMonth(TemplateSheet.DayCutoff, d.DeliveryDate) And (c.Name = d.ClientName) Then
                targetData.Add d
            End If
        Next d
        With TemplateSheet
            .Clear
            .WriteData targetData, c
            .SaveAsNewBook
        End With
    Next c
End Sub

''' Trueの場合、年月が等しい。
Public Function EqualsMonth(ByVal d1 As Date, ByVal d2 As Date) As Boolean
    EqualsMonth = (Year(d1) = Year(d2) And Month(d1) = Month(d2))
End Function
